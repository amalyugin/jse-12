package ru.t1.malyugin.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInteger() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }

}