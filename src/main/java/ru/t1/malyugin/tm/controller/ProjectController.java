package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.controller.IProjectController;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");

        final List<Project> projects = projectService.findAll();
        if (projects.isEmpty()) {
            System.out.println("[EMPTY]");
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECTS BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[PROJECT NOT FOUND]");
            return;
        }
        System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECTS BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[PROJECT NOT FOUND]");
            return;
        }
        System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");

        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }

        System.out.println("NEW PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[REMOVE PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateById(id, name, description);
        if (project == null) return;

        System.out.println("UPDATED PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) return;

        System.out.println("UPDATED PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s - STARTED%n", id);
        System.out.println("[OK]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");

        System.out.print("ENTER ID: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s - STARTED%n", project.getId());
        System.out.println("[OK]");
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.COMPLETED);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s - COMPLETED%n", id);
        System.out.println("[OK]");
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");

        System.out.print("ENTER ID: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s - COMPLETED%n", project.getId());
        System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        if (statusIndex < 0 || statusIndex >= Status.values().length) {
            System.out.println("[FAIL]");
            return;
        }
        final Status status = Status.values()[statusIndex];
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s CHANGED STATUS TO %s%n", project.getId(), status.getDisplayName());
        System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");

        System.out.print("ENTER ID: ");
        final Integer projectIndex = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        if (statusIndex < 0 || statusIndex >= Status.values().length) {
            System.out.println("[FAIL]");
            return;
        }
        final Status status = Status.values()[statusIndex];
        final Project project = projectService.changeProjectStatusByIndex(projectIndex, status);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("PROJECT %s CHANGED STATUS TO %s%n", project.getId(), status.getDisplayName());
        System.out.println("[OK]");
    }

}