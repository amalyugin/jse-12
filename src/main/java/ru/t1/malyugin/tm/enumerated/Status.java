package ru.t1.malyugin.tm.enumerated;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static String renderValuesList() {
        StringBuilder result = new StringBuilder();
        int size = Status.values().length;
        for (int i = 0; i < size; i++) {
            result.append(String.format("%d - %s, ", i, Status.values()[i].displayName));
        }
        return result.toString();
    }
}